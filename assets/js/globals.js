$(document).ready(() => {
  $('.carousel').carousel()
})

$('a.page-scroll').on('click', function (e) {
  var anchor = $(this);
  $('html, body').stop().animate({
    scrollTop: $(anchor.attr('href')).offset().top - 50
  }, 1500);
  e.preventDefault();
});

function downloadPDF(url) {
  var link = document.createElement('a');
  link.style.display = 'none';
  document.body.appendChild(link);
  link.href = url;
  link.download = url.split('/').pop();
  $(link).on('click', function () {
    document.body.removeChild(link);
  });
  link.click();
}

$(".manual-download").click(() => {
  downloadPDF("https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf")
})

